import React, { useContext } from 'react'
import { ThemeChange } from './Context'


const CountriesCards = ({ countriesData }) => {

    let {darkTheme} = useContext(ThemeChange)

    return (
        
        <div className={`mb-10 mr-5 shadow-2xl  rounded-lg overflow-hidden ${darkTheme ? 'bg-white text-black' : 'bg-[#2B3743] text-white'}`}>

            <img src={countriesData.flags.png} className='w-[100%] h-[60%] rounded-lg' />

            <h1 className='my-2 font-medium mx-3 text-[1.2rem]'>{countriesData.name.common}</h1>
            <p className='mx-3'>Population : {countriesData.population}</p>
            <p className='mx-3'>Region : {countriesData.region}</p>
            <p className='mb-2 mx-3'>Capital : {countriesData.capital ? countriesData.capital : "NA"}</p>


        </div>

    )
}

export default CountriesCards
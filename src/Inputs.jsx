import React, { useContext } from 'react'
import { ThemeChange } from './Context'

const Inputs = ({handleRegionChange,handleInputChange,continenetsData,sortByPopulation,sortByArea,subRegionsData,handleSubRegionChange,areaData, populationData}) => {

    const handleChange = (e) => {
        const region = e.target.value
        let subregionselect = document.getElementById('subRegion')
        if (region != "default"){
            subregionselect.style.display = "block"
        }else{
            subregionselect.style.display = "none"
        }
        handleRegionChange(region)
    }

    const handleInput = (e) => {
        const inputData = e.target.value
        handleInputChange(inputData.trim())
    }

    const handlePopulation = (e) =>{
        const populationSortType = e.target.value
        
        sortByPopulation(populationSortType)
    }

    const handleArea = (e) => {
        const areaSortType = e.target.value 
        sortByArea(areaSortType)
    }

    const handleSubRegion = (e) => {
        const subregion = e.target.value
        handleSubRegionChange(subregion)

    }

    let {darkTheme} = useContext(ThemeChange)
    console.log(darkTheme,"hiii")

    return (
        <div className={`${darkTheme ? 'bg-white' : 'bg-[#202D36]'}`}>
            <section className="serach-section flex justify-between px-10 py-5 mb-5 items-center mx-10">
                <div className="flex rounded-lg mb-4 mx-10" id="search-box">
                    <input type="text" placeholder="search your country" className={`py-5 w-[25vw] px-5 rounded-lg  border-none  shadow-lg ${darkTheme ? 'bg-white text-black': 'bg-[#2B3743] text-white' }`} onKeyUp={handleInput}/>
                </div>

                <select className={`px-2 mx-5 py-5 rounded-md  shadow-lg mt-[-10px] ${darkTheme ? 'bg-white text-black': 'bg-[#2B3743] text-white' }`} onChange={handlePopulation}>
                    <option value="default" selected={populationData === "default"}>Select By Population</option>
                    <option value="Ascending">Ascending</option>
                    <option value="Descending">Descending</option>
                </select>

                <select className={`px-2 mx-5 py-5 rounded-md  shadow-lg  mt-[-10px] ${darkTheme ? 'bg-white text-black': 'bg-[#2B3743] text-white' }`} onChange={handleArea}>
                    <option value="default" selected={areaData === "default"} >Select By Area</option>
                    <option value="Ascending">Ascending</option>
                    <option value="Descending">Descending</option>
                </select>


                <select className={`px-2 mx-5 py-5 rounded-md  shadow-lg  mt-[-10px] ${darkTheme ? 'bg-white text-black': 'bg-[#2B3743] text-white' }`} onChange={handleChange}>
                    
                    <option value="default">Filter By Region</option>
                    {continenetsData.map((continent) => 
                         <option value={continent} key={continent}>{continent}</option>
                    )}

                </select>

                <select id='subRegion' className={`px-2 mx-10 py-5  rounded-md  shadow-lg  mt-[-10px] ${darkTheme ? 'bg-white text-black': 'bg-[#2B3743] text-white' }`} style={{display:'none'}} onClick={handleSubRegion}>
                    <option value="default">Filter By SubRegion</option>
                        {subRegionsData.map((subregion) => 
                            <option value={subregion} key={subregion}>{subregion}</option>
                        )}
                </select>
            </section>
        </div>
    )
}

export default Inputs
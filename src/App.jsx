import React, {  useEffect, useState } from 'react'
import ThemeProvider from './Context'
import HomePage from './HomePage'

const App = () => {

  const Api_Url = "https://restcountries.com/v3.1/all"
  let [countriesData, setCountriesData] = useState([])//main

  useEffect(() => {

    async function fetchData(Api_Url) {

      try {
        const data = await fetch(Api_Url)
        const jsonData = await data.json()
        setCountriesData(jsonData)


      } catch (error) {
        console.log(error.message)
      }

    }

    fetchData(Api_Url)
  }, [])

  return (
    
    <ThemeProvider>
        <HomePage countriesData={countriesData} />
    </ThemeProvider>
  )
}

export default App